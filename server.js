// Port to listen requests from
var port = 1234;

// Modules to be used
var express = require('express');
var bodyParser = require('body-parser');
var sqlite3 = require('sqlite3').verbose();
var cookieParser =require('cookie-parser');
var path = require("path");
const 	http = require('http'),
  		url = require('url'),
  		server = http.createServer().listen(process.env.PORT || 3000);



//const baseUrl = 'http://localhost:1234';
var app = express();
var db = new sqlite3.Database('db.sqlite');

// application/x-www-form-urlencoded parser
var urlencodedParser = bodyParser.urlencoded({ extended: false });

app.use(cookieParser());

// Log requests
app.all("*", urlencodedParser, function(req, res, next){
	console.log(req.method + " " + req.url);
	console.dir(req.headers);
	console.log(req.body);
	console.log();
	next();
});
	app.get("/sessions",function(req, res, next) {
		
		db.all("SELECT * From sessions",function(err,data)
		{
				res.json(data);
		});

		});

	app.get("/",function(req, res, next) {
		
	//console.log(req.cookies);
	var getCookie = req.cookies.login_token;
	

	db.all("SELECT * FROM sessions WHERE token = ?;",[getCookie],function(err, data) {
		console.dir(data);
		console.dir(err);
		if(data.length >0)
		{
		
				console.log("redirect1");
				res.redirect('login.html');

			//	res.sendFile(path.join(__dirname,'public','login.html'));
		}
		else
		{
			console.log("redirect2");
			res.redirect('index.html');
			
			
			//res.sendFile(path.join(__dirname,'public','index.html'));
		}

	});
});

app.get("/logout", function(req, res, next) {
	res.cookie("login_token","",{maxAge: 1}); 
	var getCookie= req.cookies.login_token;
	db.all("DELETE FROM sessions WHERE token = ?;",[getCookie]);
	res.redirect('/logout.html');

	
	//dd.all('Delete ')
});

app.get("/users", function(req, res, next) {
	db.all('SELECT rowid, ident, password FROM users;', function(err, data) {
		//res.json(data);
	});
});

app.post("/login", function(req, res, next) {
	db.all('SELECT * FROM users WHERE ident =? AND password =?;', [req.body.login,req.body.mdp], function(err, data) {
		if(data.length >0)
		{
			var randtoken = require('rand-token');
			var token_generate = randtoken.generate(16);
			res.cookie("login_token",token_generate);

					db.all('SELECT * FROM sessions  WHERE ident =?;',[req.body.login],function(err, data) {

					if(err)
					{
						 throw err;
					}
					
					if(data.length == 0)
					{
						db.all('INSERT INTO sessions VALUES(?,?)',[req.body.login,token_generate],function(err, data) {

								res.redirect('/login.html');
						}
							);

						
					}
					else
					{
						db.all('UPDATE INTO sessions SET token = ? WHERE ident =? ;',[req.body.login,token_generate],function(err, data) {

								res.redirect('/login.html');
								}
							);
					}

					
			});
			
  			/*if(req.cookies.login_token == undefined)
  			{
  				res.cookie("login_token",token_generate);
  			}
  			else
  			{*/
  				
  			//}

 		
  			
 			
			
		}
		else 
		{

			res.json({status: (false)});
		}
	});
	
});

// Serve static files
app.use(express.static('public'));

// Startup server
app.listen(port, function () {
	console.log('Le serveur est accessible sur http://localhost:' + port + "/");
	console.log('La liste des utilisateurs est accessible sur http://localhost:' + port + "/users");
});
